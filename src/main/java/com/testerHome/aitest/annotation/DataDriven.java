package com.testerHome.aitest.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DataDriven {
	String[] value() default "";
	String method();
}
