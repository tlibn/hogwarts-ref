package com.testerHome.aitest;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.testerHome.aitest.util.StringUtils;
import org.junit.Ignore;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.platform.suite.api.SelectClasses;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

/**
 *  霍格沃兹测试学院-解析测试用例工具类
 * @Author tlibn
 * @Date 2019/10/8 9:44
 **/
public class ParseUtil {

    private static String PACKAGE_NAME = "packageName";
    private static String CLASS_NAME = "className";
    private static String METHOD_NAME = "methodName";
    private static String ORDER_NUM = "orderNum";

    public static void main(String[] args) throws Exception {

        String jarFile = "F:\\备课数据\\hogwarts-1.0-SNAPSHOT-fat-tests.jar";
        //refOnePackage("com.testerHome.aitest.util");
        //refOnePackage("com.testing.studio.auto.testcase2", jarFile);
        //refDetail(jarFile);

    }


}
