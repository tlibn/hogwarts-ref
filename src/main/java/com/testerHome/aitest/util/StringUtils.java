package com.testerHome.aitest.util;

/**
 *  霍格沃兹测试学院-示例
 * @Author tlibn
 * @Date 2019/11/13 19:47
 **/
public class StringUtils {

    public static boolean isBlank(String str) {
        return null == str || "".equals(str);

    }

    public static boolean isEmpty(String str) {
        return isBlank(str);

    }
}
