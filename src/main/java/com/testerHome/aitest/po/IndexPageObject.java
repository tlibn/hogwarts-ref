package com.testerHome.aitest.po;

import com.testerHome.aitest.annotation.AiTest;
import com.testerHome.aitest.annotation.MyOrder;
import com.testerHome.aitest.annotation.MyTest;

/**
 *  霍格沃兹测试学院-示例
 * @Author tlibn
 * @Date 2019/10/8 9:44
 **/
@AiTest
public class IndexPageObject extends BasePageObject {

    public static void Test4(Long time) {

    }
    public void Test5( Long time, String name) {

    }

    /**
     *
     * @param time
     * @param name
     */
    public StringBuilder Test5(@MyTest long time, String name) {

        return new StringBuilder();

    }
    public static void Test5(String name) {

    }
    public void Test6(Long time, @MyTest String name) {

    }


}
