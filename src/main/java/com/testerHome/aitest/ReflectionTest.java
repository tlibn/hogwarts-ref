package com.testerHome.aitest;

import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.MethodParameterScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ConfigurationBuilder;

import com.testerHome.aitest.po.BasePageObject;
import com.testerHome.aitest.annotation.AiTest;
import com.testerHome.aitest.annotation.MyTest;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Set;

/**
 *  霍格沃兹测试学院-示例
 * @Author tlibn
 * @Date 2020/1/10 17:13
 **/
public class ReflectionTest {
    public static void main(String[] args) {

        String packageName = "com.testerHome.aitest.po";
        // 初始化Reflections
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .forPackages(packageName) // 指定路径URL
                //.addScanners(new SubTypesScanner()) // 添加子类扫描工具
                .addScanners(new FieldAnnotationsScanner()) // 添加属性注解扫描工具
                .addScanners(new MethodAnnotationsScanner()) // 添加方法注解扫描工具
                .addScanners(new MethodParameterScanner()) // 添加方法参数扫描工具
        );

        // 反射出子类
        Set<Class<? extends BasePageObject>> subClassSet = reflections.getSubTypesOf( BasePageObject.class ) ;
        //System.out.println("反射出子类:" + subClassSet);

        // 反射出带有指定注解的类
        Set<Class<?>> annotatedSet = reflections.getTypesAnnotatedWith( AiTest.class ,true);
        System.out.println("反射出带有指定注解的类:" + annotatedSet);

        // 获取带有特定注解对应的方法
        Set<Method> methods = reflections.getMethodsAnnotatedWith( AiTest.class ) ;
        //System.out.println("获取带有特定注解对应的方法:" + methods);

        // 获取带有特定注解对应的字段
        Set<Field> fields = reflections.getFieldsAnnotatedWith( MyTest.class ) ;
        //System.out.println("获取带有特定注解对应的字段:" + fields);

        // 获取特定参数对应的方法
        Set<Method> someMethods = reflections.getMethodsMatchParams(long.class, String.class);
        //System.out.println("获取特定参数对应的方法:" + someMethods);

        // 获取特定返回值的方法
        Set<Method> voidMethods = reflections.getMethodsReturn(StringBuilder.class);
        //System.out.println( "获取特定返回值的方法:" + voidMethods);

        // 返回使用特定注解修饰参数的方法
        Set<Method> pathParamMethods =reflections.getMethodsWithAnyParamAnnotated( MyTest.class);
        //System.out.println("返回使用特定注解修饰参数的方法:" + pathParamMethods);
    }
}
