package com.testerHome.aitest;

import com.testerHome.aitest.annotation.DataDriven;
import com.testerHome.aitest.annotation.MyOrder;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.reflect.Method;
import java.util.*;

/**
 *  霍格沃兹测试学院-数据驱动测试类
 * @Author tlibn
 * @Date 2020/01/10 9:44
 **/
public class DataDrivenUtil {

    public static void main(String[] args) throws Exception {
        parsePackage("com.testerHome.aitest.test");
    }

    public static void parsePackage(String packageName) throws Exception {

        //初始化Reflections
        Reflections reflections = getReflections(packageName);

        //获取被DataDriven注解修饰的方法
        Set<Method> methods = reflections.getMethodsAnnotatedWith(DataDriven.class);
        Set<Method> myOrderMethods = reflections.getMethodsAnnotatedWith(MyOrder.class);

        for (Method tempMethod :methods) {

            //获取方法所在的类
            Class<?> classType = tempMethod.getDeclaringClass();
            //获取方法上面DataDriven注解中的属性值
            DataDriven dataDriven = tempMethod.getAnnotation(DataDriven.class);
            String dataDrivenMethodName = dataDriven.method();
            //通过反射获取本类中的方法
            Method dataDrivenMethod = classType.getDeclaredMethod(dataDrivenMethodName);

            //通过反射运行该方法并获取返回值
            List<List> dataDrivenList = (List<List>)dataDrivenMethod.invoke(classType.newInstance());
            //根据数据驱动方法循环执行待测试方法
            for (List data:dataDrivenList) {
                tempMethod.invoke(classType.newInstance(),data.toArray());
            }
        }
    }

    /**
     *  初始化Reflections
     * @param packageName
     * @return
     */
    public static Reflections getReflections(String packageName) {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage(packageName))
                .filterInputsBy(new FilterBuilder().includePackage(packageName))
                .setScanners(new MethodAnnotationsScanner(), new SubTypesScanner(), new TypeAnnotationsScanner()));
        return reflections;
    }

}
