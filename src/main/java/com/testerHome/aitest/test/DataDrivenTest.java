package com.testerHome.aitest.test;

import com.testerHome.aitest.annotation.DataDriven;
import com.testerHome.aitest.annotation.MyOrder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@DisplayName("霍格沃兹测试学院-数据驱动测试示例")
public class DataDrivenTest {

    @BeforeEach
    public void before(){
        System.out.println("====== @BeforeEach =======");
    }

    @DisplayName("数据驱动测试")
    @DataDriven(method = "testData")
    //@Test
    @MyOrder(2)
    public void checkUserInfo(String name,String addr,Integer age) {
        System.out.println("====== 数据驱动测试 start =======");
        System.out.println("姓名："+name);
        System.out.println("地址："+addr);
        System.out.println("年龄："+age);
        System.out.println("====== 数据驱动测试 end =======");
    }

    @MyOrder(1)
    public void checkUserInfo2(String name,String addr,Integer age) {
        System.out.println("====== 数据驱动测试 start =======");
        System.out.println("姓名："+name);
        System.out.println("地址："+addr);
        System.out.println("年龄："+age);
        System.out.println("====== 数据驱动测试 end =======");
    }

    public List<List> testData() {

        List list = new ArrayList<>();

        List<List> list2 = new ArrayList(){
            {
                add("霍格沃兹");
                add("北京市");
                add(1000);
            }
        };
        list.add(list2);

        List list3 = new ArrayList(){
            {
                add("霍格沃兹测试学院");
                add("北京市昌平区");
                add(100);
            }
        };
        list.add(list3);

        List list4 = new ArrayList(){
            {
                add("霍小测");
                add("北京市昌平区霍格沃兹测试学院");
                add(201);
            }
        };
        list.add(list4);

        return list;
    }
}